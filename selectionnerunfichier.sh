#!/bin/bash

# Ouvrir une boîte de dialogue pour sélectionner un fichier
fichier=$(zenity --file-selection)

# Vérifier si un fichier a été sélectionné
if [ -n "$fichier" ]; then
  # Un fichier a été sélectionné, traiter le fichier ici
  echo "Vous avez sélectionné le fichier : $fichier"
  # Exemple de traitement du fichier
  # ...
else
  # Aucun fichier n'a été sélectionné
  echo "Aucun fichier n'a été sélectionné."
fi
