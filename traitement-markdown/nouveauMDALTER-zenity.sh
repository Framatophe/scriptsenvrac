#!/bin/bash
# Creer un document markdown avec un en-tete
# on utilise ce script en faisant bash nomduscript.sh
# On va utiliser Zenity pour sélectionner des fichiers

#Variables par défaut
# On demande le titre du document qui servira aussi de nom de fichier
read -p "Entrez le Titre : " letitre
letitre=${letitre:-Titre}
echo "Le nom final du fichier comportera une variante du titre que vous avez entré : $letitre"


read -p "Maintenant je vous propose de sélectionner le fichier de bibliographie. Souhaitez-vous le faire ? si oui nous choisirons un fichier, si non, nous sautons cette étape (y, n) " reponsebib
		case $reponsebib in
			[Yy] )	# Ouvrir une boîte de dialogue pour sélectionner un fichier
				fichierbib=$(zenity --file-selection)

				# Vérifier si un fichier a été sélectionné
				if [ -n "$fichierbib" ]; then
  				# Un fichier a été sélectionné, traiter le fichier ici
  				echo "Vous avez sélectionné le fichier : $fichierbib"
	  			else
	  			# Aucun fichier n'a été sélectionné
	  			echo "Aucun fichier n'a été sélectionné."
	  			fi ;;
			[Nn] ) echo "OK on n'utilise pas de fichier biblio" ; break ;;
			* ) echo réponse invalide;
			exit 1;;
		esac


read -p "Ensuite, je vous propose de sélectionner le fichier de style CSL. Souhaitez-vous le faire ? si oui nous choisirons un fichier, si non, nous sautons cette étape (y, n) " reponsecsl
		case $reponsecsl in
			[Yy] ) 
				# Ouvrir une boîte de dialogue pour sélectionner un fichier
				fichiercsl=$(zenity --file-selection)

				# Vérifier si un fichier a été sélectionné
				if [ -n "$fichiercsl" ]; then
  				# Un fichier a été sélectionné, traiter le fichier ici
  				echo "Vous avez sélectionné le fichier : $fichiercsl"
	  			else
	  			# Aucun fichier n'a été sélectionné
	  			echo "Aucun fichier n'a été sélectionné."
	  			fi ;;
			[Nn] ) echo "OK on n'utilise pas de fichier CSL" ; break ;;
			* ) echo réponse invalide;
			exit 1;;
		esac

# Fonction qui va créer l'en-tête et le début du document
generate_output() {
echo "---"
echo "title: $letitre" # La variable pour le titre
echo "author: Christophe Masutti"
echo "date: $(date +"%d-%m-%Y")" # On utilise la date courante
echo "lang: fr"
echo "csl: $fichiercsl"
echo "bibliography: $fichierbib"
echo "description: complétez cette ligne pour décrire votre document (le résumé, par exemple)."
echo "---"
echo "# Première section du document"
echo
echo
echo
echo
echo
echo "# Bibliographie"
}

# Le titre devient un argument
title="$letitre"

# On transforme en bas de casse et on remplace les espaces par des tirets
filename=$(echo "$title" | tr '[:upper:]' '[:lower:]' | tr ' ' '-')

# Création du fichier markdown
output_file="$filename.md"
generate_output "$title" > "$output_file"

echo -e " ***********************************\n Le fichier Markdown intitulé \n '$output_file' \n avec un en-tête Yaml a été créé ! \n félicitations !\n ***********************************"



