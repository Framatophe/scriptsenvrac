#!/bin/bash
# Transformer un document markdown
# utiliser la commande bash transformation.sh

# On demande le nom du fichier on verifie s'il existe
while true; do
	read -p "Quel fichier voulez-vous transformer? : " lefichier
	lefichier=${lefichier:-lefichier}
	filename='$lefichier'

	if [ -f "$lefichier" ]; then
    		echo "OK. Le fichier $lefichier existe."
    	break
	else
		echo "Ha non, ce fichier n'existe pas"
	fi
done

	read -p "Voulez-vous le transformer en ODT ou en HTML (odt, html) ? : " extension
	if [[ $extension == "odt" || $extension == "html" ]]
  	then
      		pandoc -f markdown -t $extension --citeproc $lefichier -s -o $lefichier-traitement.$extension

      	else
      		echo "$extension n'est pas un format accepté par ce programme"
  	fi    





