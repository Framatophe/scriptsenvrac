#!/bin/bash
# Creer un document markdown avec un en-tete
# on utilise ce script en faisant bash nomduscript.sh
# On demande le titre du document qui servira aussi de nom de fichier
read -p "Entrez le Titre : " letitre
letitre=${letitre:-Titre}
echo $letitre

# On demande le nom du fichier de biblio et on verifie s'il existe
while true; do
	read -p "Entrez le nom du fichier de bibliographie : " biblio
	biblio=${biblio:-biblio}
	filename='$biblio'

	if [ -f "$biblio" ]; then
    	echo "OK. Le fichier existe."
    	break
	else
	echo "Ha non, le fichier n existe pas"
	fi
done
echo $biblio


# Fonction qui va créer l'en-tête et le début du document
generate_output() {
echo "---"
echo "title: $letitre" # La variable pour le titre
echo "author: Christophe Masutti"
echo "date: $(date +"%d-%m-%Y")" # On utilise la date courante
echo "csl: framatophe-biblio-iso-690-20240531.csl"
echo "bibliography: $biblio"
echo "description: complétez cette ligne pour décrire votre document (le résumé, par exemple)."
echo "---"
echo "# Première section du document"
echo
echo
echo
echo
echo
echo "# Bibliographie"
}

# Le titre devient un argument
title="$letitre"

# On transforme en bas de casse et on remplace les espaces par des tirets
filename=$(echo "$title" | tr '[:upper:]' '[:lower:]' | tr ' ' '-')

# Création du fichier markdown
output_file="$filename.md"
generate_output "$title" > "$output_file"

echo "Le fichier Markdown intitulé '$output_file' avec un en-tête Yaml a été créé ! félicitations !"


