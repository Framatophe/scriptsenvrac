#!/bin/bash
# Creer un document markdown avec un en-tete
# on utilise ce script en faisant bash nomduscript.sh
# On demande le titre du document qui servira aussi de nom de fichier
read -p "Entrez le Titre : " letitre
letitre=${letitre:-Titre}
echo "Le nom final du fichier comportera une variante du titre que vous avez entré : $letitre"

# On demande le nom du fichier de biblio et on verifie s'il existe
while true; do
	read -p "Entrez le nom du fichier de bibliographie (avec son extension, du genre mabiblio.json ou mabiblio.bib) : " biblio
	biblio=${biblio:-biblio}
	filename='$biblio'

	if [ -f "$biblio" ]; 
	then
    		echo "OK. Le fichier existe."
    	break
	else
	read -p "Attention, ce fichier n'existe pas dans le répertoire. Souhaitez-vous le créer (vide) néanmoins ? (y, n) " reponse
		case $reponse in
			[Yy] ) touch $biblio ; break;;
			[Nn] ) echo "OK on ne crée pas de fichier $biblio" ; break ;;
			* ) echo réponse invalide;
			exit 1;;
		esac
	fi
done

# On regarde s'il y a un fichier CSL 

# Extension du fichier à rechercher (modifier si nécessaire)
extension_csl="csl"

# Rechercher tous les fichiers csl dans le répertoire courant avec l'extension spécifiée
fichiers_csl=$(find . -type f -name "*.$extension_csl")

# Vérifier si des fichiers csl ont été trouvés
if [ -n "$fichiers_csl" ]; then
  # Des fichiers ont été trouvés, traiter les fichiers ici
  for unfichier_csl in $fichiers_csl; do
    echo "Le fichier $unfichier_csl a été trouvé dans le répertoire courant"
  done
else
  # Aucun fichier n'a été trouvé, afficher un message d'avertissement
  echo "Attention : aucun fichier avec l'extension $extension_csl n'a été trouvé dans le répertoire courant."
fi


# On demande le nom du fichier csl à utiliser et on verifie s'il existe
while true; do
	read -p "Entrez le nom du fichier csl à utiliser (avec son extension, du genre style.csl) : " stylecsl
	stylecsl=${stylecsl:-stylecsl}
	filename='$stylecsl'

	if [ -f "$stylecsl" ]; 
	then
    		echo "OK. Le fichier existe."
    	break
	else
	read -p "Attention, ce fichier stylecsl n'existe pas dans le répertoire. Souhaitez-vous le créer (vide) néanmoins ? (y, n) " reponse
		case $reponse in
			[Yy] ) touch $stylecsl ; break;;
			[Nn] ) echo "OK on ne crée pas de fichier $stylecsl" ; break ;;
			* ) echo réponse invalide;
			exit 1;;
		esac
	fi
done


# Fonction qui va créer l'en-tête et le début du document
generate_output() {
echo "---"
echo "title: $letitre" # La variable pour le titre
echo "author: Christophe Masutti"
echo "date: $(date +"%d-%m-%Y")" # On utilise la date courante
echo "lang: fr"
echo "csl: $stylecsl"
echo "bibliography: $biblio"
echo "description: complétez cette ligne pour décrire votre document (le résumé, par exemple)."
echo "---"
echo "# Première section du document"
echo
echo
echo
echo
echo
echo "# Bibliographie"
}

# Le titre devient un argument
title="$letitre"

# On transforme en bas de casse et on remplace les espaces par des tirets
filename=$(echo "$title" | tr '[:upper:]' '[:lower:]' | tr ' ' '-')

# Création du fichier markdown
output_file="$filename.md"
generate_output "$title" > "$output_file"

echo -e " ***********************************\n Le fichier Markdown intitulé \n '$output_file' \n avec un en-tête Yaml a été créé ! \n félicitations !\n ***********************************"



