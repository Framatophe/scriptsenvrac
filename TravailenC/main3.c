#include <stdio.h>

int main(void)

{

/*
+ (addition)
- (soustraction)
* (multiplication
/ (division) 5/2 = 2 (pas de reste car entier sauf si float)
% (modulo) 5 % 2 = 1 (le reste de la division euclidienne précédente)
*/

	int calcul = 6 + 4;
	int nombre1 = 9;
	int nombre2 = 13;
	printf("%d\n", calcul);
	// on peut aussi carément se passer de variable
	printf("%d\n", 4+5);
	// et on peut calculer des variables
	printf("%d\n", nombre1 * nombre2);

	return 0;
}
