#include <stdio.h>

int main(void)

/*
	MaVariable : contenu de la variable (exemple: maVariable = 15)
	&maVariable : adresse où est stockée la variable
*/


{
	int ageUtilisateur = 0; /// j'affecte la valeur par défaut à la variable
	signed char uneLettre = 'A';

	printf("Quel age avez-vous ? ");
	scanf("%d", &ageUtilisateur);  //scanf est un peu l'inverse de printf, elle n'est pas securisée, donc attention

	printf("Vous avez %d ans.\n", ageUtilisateur);
	

	return 0;
}


