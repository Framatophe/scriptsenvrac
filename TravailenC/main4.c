#include <stdio.h>

int main(void)

{

// on peut changer l'affectation de variable avec des calcul. Exemple avec des niveaux de joueur.
	int niveau_joueur = 0;
	printf("Quel est le niveau de départ ? : ");
	scanf("%d", &niveau_joueur);
	
	printf("Niveau de départ : %d\n", niveau_joueur);
	
	// le personnage a fait des quêtes...

	niveau_joueur = niveau_joueur + 1;
	printf("Après avoir brillamment réussi la quête du champignon magique,\n");
	printf("le niveau du joueur est désormais de  : %d\n", niveau_joueur);

	return 0;
}

/*
On peut retenir que écrire deux fois le nom d'une variable
peut être long. Donc quand on a 
<variable> = <variable> + quelquechose 
on peut écrire
<variable> += quelquechose
Dans le cas ci-dessus : niveau_joueur += 1
idem pour tous les opérateurs

Remarque : pour l'incrémentation positive ou négative de 1
On peut faire ainsi (la variable est A) :
A += 1 --> A++ (post-incrémentation) ou ++A (pré-incrémentation) 
A -= 1 --> A-- (post-décrémentation) ou --A (pré-décrémentation)

Dans notre exemple niveau_joueur++ incrémente de 1 la variable.

*/
