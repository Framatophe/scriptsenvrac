#include <stdio.h>

/*
*  voici un commentaire qui
*  tient sur plusieurs lignes,
*  c'est comme on veut
*/

/*
%d : nombre entier (int)
%f : nombre flottant (float)
%c : caractère (char)
%s : chaîne de caractères (texte)
*/

int main(void) // ceci est un commentaire sur une seule ligne
{
	int mon_premier_nombre_entier = 45; // on déclare (et on affecte)  une variable de type entier (int) avec une valeur
	int mon_second_nombre_entier = 124;
	float un_prix = 345.67;
	const float PI = 3.14; // cette valeur ne pourra jamais être changée dans la suite du programme (const-ante)
	printf("Le nombre  est %d ou %d\n", mon_premier_nombre_entier, mon_second_nombre_entier);
	printf("Le prix s'affiche avec trop de décimales : %f\n", un_prix);
	printf("Il faut préciser qu'on veut deux décimales : %.2f\n", un_prix);
	printf("La valeur de PI est toujours de %.2f\n", PI);
	return 0;
}
