while 1: # 1 est toujours vrai -> boucle infinie
    lettre = input("Tapez 'Q' pour quitter ou n'importe quelle touche pour continuer: ")
    if lettre == "Q":
        print("Fin de la boucle")
        break
    else:
    # Ouverture d'un fichier en *ajout*:
        fichier = open("/home/bonlohi/Documents/tests/biblio.bib", "a")
        fichier.write('\n')
        choix = input('(L)ivre, (A)rticle ?')

    if choix == 'L':
        titrelivre = input('Entrez le titre : ')
        prenomauteurlivre = input('Entrez le prénom de l\'auteur : ')
        nomauteurlivre = input('Entrez le nom de l\'auteur : ')
        editionlivre = input('Entrez la maison d\'édition : ')
        anneelivre = input('Entrez l\'année de parution : ')
        fichier.write('@book{')
        fichier.write(nomauteurlivre)
        fichier.write(anneelivre)
        fichier.write(',')
        fichier.write('\n\t')
        fichier.write('author = ')
        fichier.write('"')
        fichier.write(nomauteurlivre)
        fichier.write(', ')
        fichier.write(prenomauteurlivre)
        fichier.write('"')
        fichier.write(',')
        fichier.write('\n\t')
        fichier.write('title = ')
        fichier.write('"')
        fichier.write(titrelivre)
        fichier.write('"')
        fichier.write(',')
        fichier.write('\n\t')
        fichier.write('publisher = ')
        fichier.write('"')
        fichier.write(editionlivre)
        fichier.write('"')
        fichier.write(',')
        fichier.write('\n\t')
        fichier.write('year = ')
        fichier.write('"')
        fichier.write(anneelivre)
        fichier.write('"')
        fichier.write(',')
        fichier.write('\n')
        fichier.write('}')
    
    else:
        print('Entrez les bonnes lettres')

    # Fermeture du fichier
    fichier.close()
