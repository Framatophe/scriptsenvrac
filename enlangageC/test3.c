#include <gtk/gtk.h>

void close_window(GtkWidget *widget, gpointer data);
void create_text_file(GtkWidget *widget, GtkWidget *hbox, gpointer data);

int main(int argc, char *argv[]) {
    // ... (GTK initialization and window creation remains the same) ...

    // Create a text entry field
    GtkWidget *entry = gtk_entry_new_with_max_length(400);

    // Create an "Action" button
    GtkWidget *action_button = gtk_button_new_with_label("Action");
    g_signal_connect(action_button, "clicked", G_CALLBACK(create_text_file), entry);

    // Add the text entry field and button to the horizontal box
    gtk_box_pack_start(GTK_BOX(hbox), entry, TRUE, TRUE, 0);
    gtk_box_pack_end(GTK_BOX(hbox), action_button, FALSE, FALSE, 0);


    // ... (Rest of the code remains the same) ...
}

void create_text_file(GtkWidget *widget, gpointer data) {
    // Get the text from the entry field
    GtkWidget *entry = (GtkWidget *) data;
    const char *text = gtk_entry_get_text(GTK_ENTRY(entry));

    // Create a file name based on the current timestamp
    time_t now = time(NULL);
    struct tm *timeinfo = localtime(&now);
    char filename[32];
    strftime(filename, sizeof(filename), "text_%Y%m%d_%H%M%S.txt", timeinfo);

    // Open the file in write mode
    FILE *file = fopen(filename, "w");
    if (!file) {
        // Handle error opening the file
        g_print("Error opening file: %s\n", filename);
        return;
    }

    // Write the text to the file
    fprintf(file, "%s", text);
    fclose(file);

    // Display a success message
    g_print("File saved successfully: %s\n", filename);
}

