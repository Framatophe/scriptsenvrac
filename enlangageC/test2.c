#include <gtk/gtk.h>

//à compiler avec gcc test2.c -o test2 `pkg-config --cflags --libs gtk+-3.0`
void close_window(GtkWidget *widget, gpointer data) {
  gtk_main_quit();
}

int main(int argc, char *argv[]) {
  // Initialize GTK
  gtk_init(&argc, &argv);

  // Create the main window
  GtkWidget *window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(window), "Bonjour");
  gtk_window_set_default_size(GTK_WINDOW(window), 200, 100);

  // Create the "Bonjour" label
  GtkWidget *label = gtk_label_new("Bonjour");
  gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_CENTER);

  // Create the "Close" button
  GtkWidget *button = gtk_button_new_with_label("Fermer");
  g_signal_connect(button, "clicked", G_CALLBACK(close_window), NULL);

  // Create a horizontal box to arrange the label and button
  GtkWidget *hbox = gtk_box_new(FALSE, 5);

  // Add the label to the horizontal box
  gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);

  // Add the button to the horizontal box (right-aligned)
  gtk_box_pack_end(GTK_BOX(hbox), button, FALSE, FALSE, 0);

  // Add the horizontal box to the window
  gtk_container_add(GTK_CONTAINER(window), hbox);

  // Show all widgets
  gtk_widget_show_all(window);

  // Start the GTK main loop
  gtk_main();

  return 0;
}

