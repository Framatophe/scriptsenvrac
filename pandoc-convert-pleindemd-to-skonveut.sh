# Ce script permet de convertir avec pandoc plusieurs
# fichiers de tel format en autant de fichiers d'autres formats 
# il suffit de changer les extensions
# placer les fichiers dans un répertoire
# lancer ce script dedans

FILES=*.md
for f in $FILES
do
  # extension="${f##*.}"
  filename="${f%.*}"
  echo "Je convertis $f en $filename.odt"
  `pandoc $f -t odt -o $filename.odt`
  # décommenter la ligne ci-dessous si vous voulez détruire le fichier source
  # rm $f
done