import pandas as pd
from pandas import DataFrame

collec = pd.read_csv("collection.csv",sep = '|')
collec = collec.sort_values(by=['Serie'],ascending=True)
collec.to_csv('sortie.csv',sep='|',index = False)
