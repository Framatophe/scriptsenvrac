import pandas as pd

columns = ['Série', 'Titre', 'Tome', 'Scénariste', 'Dessinateur', 'Éditeur']
df = pd.read_csv('sortie.csv', names=columns,sep = '|')


# Use the .to_html() to get your table in html
df.to_html('sortie.html',index=True)
